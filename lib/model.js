const MongoClient = require('mongodb').MongoClient;
const url = "mongodb://localhost:27017/";

const moment = require('moment');

module.exports = {
    productsDetailt: () => {
        return new Promise((resolve, reject) => {
            MongoClient.connect(url, { useNewUrlParser: true, useUnifiedTopology: true}, (err, db) => {
                if (err) reject(err);
                const dbo = db.db("test_db");
                dbo.collection("products").find({}).toArray((err, result) => {
                    if (err) reject(err);
                    resolve(result);
                    db.close();
                });
            });
        });
    },

    usersViewDetails: (id, filter, reqURL) => {
        return new Promise((resolve, reject) => {
            MongoClient.connect(url, { useNewUrlParser: true, useUnifiedTopology: true }, (err, db) => {
                if (err) reject(err);
        
                const dbo = db.db("test_db");
        
                let query = { productId: id };
        
                dbo.collection("userView").find(query).toArray((err, result) => {
                    if (err) reject(err);
        
                    let uniqueViews = [];
        
                    result.filter(item => {
                        let i = uniqueViews.findIndex(x => (x.userId == item.userId));
                        return i <= -1 ? uniqueViews.push(item) : null;
                    });
        
                    if (filter) {
                        if (filter === 'daily') {
                            let todayDate =  new Date().toJSON();
                            todayDate = todayDate.slice(0, todayDate.indexOf('T'));
                            
                            uniqueViews = uniqueViews.filter(view => view.viewDate === todayDate);
                        } else if (filter === 'weekly') {                    
                            let startOfWeek = moment().startOf("isoWeek").toDate().toJSON();
                            startOfWeek = startOfWeek.slice(0, startOfWeek.indexOf('T'));
        
                            let endOfWeek = moment().endOf("isoWeek").toDate().toJSON();
                            endOfWeek = endOfWeek.slice(0, endOfWeek.indexOf('T'));
        
                            uniqueViews = uniqueViews.filter(view => new Date(view.viewDate) > new Date(startOfWeek) && new Date(view.viewDate) <= new Date(endOfWeek));
                        } else if (filter === 'monthly') {
                            let startOfMonth = moment().startOf('month').format('YYYY-MM-DD');
                            let endOfMonth   = moment().endOf('month').format('YYYY-MM-DD');
        
                            uniqueViews = uniqueViews.filter(view => new Date(view.viewDate) > new Date(startOfMonth) && new Date(view.viewDate) <= new Date(endOfMonth));
                        } else if (!isNaN(parseInt(filter))) {
                            let fromDate = reqURL.split('=')[1].split('/')[0];
                            let toDate = reqURL.split('=')[1].split('/')[1];
                            if (fromDate <= toDate) {
                                uniqueViews = uniqueViews.filter(view => new Date(view.viewDate) >= new Date(fromDate) && new Date(view.viewDate) <= new Date(toDate));
                            }
                        }
                    }
        
                    db.close();

                    resolve(uniqueViews);
                });
            });
        });
    }
}