const express = require("express");
const app = express();
const model = require('./lib/model');

app.set("view engine", "ejs");
app.set("views", "pages");

const bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({ extended: false })); 
app.use(bodyParser.json());


app.get("/", (req, res) => {
    return res.redirect("/products");
});

app.get("/products", (req, res)=>{
    model.productsDetailt()
         .then(r => {
            res.render("products", { result: r });
         })
         .catch(err => console.log(err.message));
});

app.get("/products/:id/views", (req, res) => {  
    let id = req.params.id;
    let filter = req.query.filter;
    let reqURL = req.url;

    model.usersViewDetails(id, filter, reqURL)
         .then(r => {
            res.render('users', { result: r });
         })
         .catch(err => console.log(err.message));
});

app.listen(3200, () => {
    console.log('server started at port 3200');
});



